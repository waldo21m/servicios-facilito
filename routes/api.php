<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    "prefix" => "v1",
    "namespace" => "Api\V1",
    "middleware" => ["auth:api"],
], function () {
    Route::apiResources([
        'posts' => 'PostController',
        'users' => 'UserController',
        'comments' => 'CommentController',
    ]);

    Route::get('/posts/{post}/relationships/author', 'PostRelationshipController@author')->name('posts.relationships.author');

    Route::get('/posts/{post}/author', 'PostRelationshipController@author')->name('posts.author');

    Route::get('/posts/{post}/relationships/comments', 'PostRelationshipController@comments')->name('posts.relationships.comments');

    Route::get('/posts/{post}/comments', 'PostRelationshipController@comments')->name('posts.comments');

    Route::get('/users/{user}/relationships/posts', 'UserRelationshipController@posts')->name('users.relationships.posts');

    Route::get('/users/{user}/posts', 'UserRelationshipController@posts')->name('users.posts');

    Route::get('/users/{user}/relationships/comments', 'UserRelationshipController@comments')->name('users.relationships.comments');

    Route::get('/users/{user}/comments', 'UserRelationshipController@comments')->name('users.comments');

    Route::get('/comments/{comment}/relationships/post', 'CommentRelationshipController@post')->name('comments.relationships.post');

    Route::get('/comments/{comment}/post', 'CommentRelationshipController@post')->name('comments.post');

    Route::get('/comments/{comment}/relationships/user', 'CommentRelationshipController@user')->name('comments.relationships.user');

    Route::get('/comments/{comment}/user', 'CommentRelationshipController@user')->name('comments.user');
});

Route::post('login', 'Api\AuthController@login');
Route::post('signup', 'Api\AuthController@signup');

Route::get('signature', function () {
    return response()->json([
        "made_by" => [
            "name" => "Eduardo Márquez",
            "dni" => "V21343328",
            "country" => "Venezuela",
            "state" => "Caracas",
            "address" => "Av. Ppal de Altavista, Conjunto Res. La Fundación, I Etapa, Res. 6, PB, Apto. 1-D",
            "phone" => "+584142409579",
            "university" => "UNEXPO L.C.M",
        ],
        "special_thanks" => "All my family, my girlfriend, my friends and the codigo facilito staff",
    ]);
});