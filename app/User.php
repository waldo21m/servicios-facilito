<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->admin;
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'author_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function isPostsLoaded()
    {
        return $this->relationLoaded('posts');
    }

    public function isCommentsLoaded()
    {
        return $this->relationLoaded('comments');
    }
}
