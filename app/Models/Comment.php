<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content', 'user_id', 'post_id'];

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function isPostLoaded()
    {
        return $this->relationLoaded('post');
    }

    public function isUserLoaded()
    {
        return $this->relationLoaded('user');
    }
}
