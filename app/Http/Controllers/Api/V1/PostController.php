<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\PostCollection
     */
    public function index()
    {
        $posts = Post::with(['author', 'comments'])->paginate(1);

        return $posts->count() ? new PostCollection($posts) : response()->json(["data" => "There are no registered posts"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\PostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PostRequest $request)
    {
        PostResource::withoutWrapping();

        $post = Post::create($request->all());

        //return new PostResource($post);

        // En caso de querer cambiar el estatus 201 por otro.
        return (new PostResource($post))->response()->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post $post
     * @return \App\Http\Resources\PostResource
     */
    public function show(Post $post)
    {
        PostResource::withoutWrapping();

        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Post $post
     * @return \App\Http\Resources\PostResource
     */
    public function update(Request $request, Post $post)
    {
        PostResource::withoutWrapping();

        $post->update($request->all());

        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return response(null, 204);
    }
}
