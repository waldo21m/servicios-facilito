<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\CommentResource;
use App\Http\Resources\PostResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserRelationshipController extends Controller
{
    /**
     * @param \App\User $user
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function posts(User $user)
    {
        return PostResource::collection($user->posts);
    }

    /**
     * @param \App\User $user
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function comments(User $user)
    {
        return CommentResource::collection($user->comments);
    }
}
