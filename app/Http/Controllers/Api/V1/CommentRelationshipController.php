<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentRelationshipController extends Controller
{
    /**
     * @param \App\Models\Comment $comment
     * @return \App\Http\Resources\PostResource
     */
    public function post(Comment $comment)
    {
        return new PostResource($comment->post);
    }

    /**
     * @param \App\Models\Comment $comment
     * @return \App\Http\Resources\UserResource
     */
    public function user(Comment $comment)
    {
        return new UserResource($comment->user);
    }
}
