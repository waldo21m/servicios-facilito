<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\UserCollection
     */
    public function index()
    {
        return new UserCollection(User::with(['posts', 'comments'])->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     * @return \App\Http\Resources\UserResource
     */
    public function store(UserRequest $request)
    {
        UserResource::withoutWrapping();

        $user = User::create($request->all());

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \App\Http\Resources\UserResource
     */
    public function show(User $user)
    {
        UserResource::withoutWrapping();

        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UserEditRequest $request
     * @param  \App\User $user
     * @return \App\Http\Resources\UserResource
     */
    public function update(UserEditRequest $request, User $user)
    {
        UserResource::withoutWrapping();

        $user->update($request->all());

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response(null, 204);
    }
}
