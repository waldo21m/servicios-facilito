<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\CommentCollection
     */
    public function index()
    {
        $comments = Comment::with(['post', 'user', 'user.comments', 'user.posts'])->paginate();

        return $comments->count() ? new CommentCollection($comments) : response()->json(["data" => "There are no registered comments"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CommentRequest $request
     * @return \App\Http\Resources\CommentResource
     */
    public function store(CommentRequest $request)
    {
        CommentResource::withoutWrapping();

        $comment = Comment::create($request->all());

        return new CommentResource($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment $comment
     * @return \App\Http\Resources\CommentResource
     */
    public function show(Comment $comment)
    {
        CommentResource::withoutWrapping();

        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Comment $comment
     * @return \App\Http\Resources\CommentResource
     */
    public function update(Request $request, Comment $comment)
    {
        CommentResource::withoutWrapping();

        $comment->update($request->all());

        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response(null, 204);
    }
}
