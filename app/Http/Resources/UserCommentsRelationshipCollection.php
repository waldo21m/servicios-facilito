<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCommentsRelationshipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->additional["user"];

        return [
            'links' => [
                'self' => route('users.relationships.comments', ['user' => $user]),
                'related' => route('users.comments', ['user' => $user]),
            ],

            'data' => CommentIdentifierResource::collection($this->collection),
        ];
    }
}
