<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->getTable(),
            'id' => $this->id,
            'attributes' => [
                'content' => $this->content,
            ],

            $this->mergeWhen(($this->isPostLoaded() && $this->isUserLoaded()), [
                'relationships' => new CommentsRelationshipResource($this),
            ]),

            'links' => [
                'self' => route('comments.show', ['comment' => $this->id])
            ]
        ];
    }
}
