<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommentsRelationshipResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'post' => [
                'links' => [
                    'self' => route('comments.relationships.post', ['comment' => $this->id]),
                    'related' => route('comments.post', ['comment' => $this->id]),
                ],
                'data' => new PostIdentifierResource($this->post),
            ],

            'user' => [
                'links' => [
                    'self' => route('comments.relationships.user', ['comment' => $this->id]),
                    'related' => route('comments.user', ['comment' => $this->id]),
                ],
                'data' => new UserIdentifierResource($this->user),
            ],
        ];
    }
}
