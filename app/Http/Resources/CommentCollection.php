<?php

namespace App\Http\Resources;

use App\Models\Post;
use App\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CommentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function toArray($request)
    {
        return CommentResource::collection($this->collection);
    }

    public function with($request)
    {
        $posts = $this->collection->map(function ($comment) {
            return $comment->post;
        });

        $users = $this->collection->map(function ($comment) {
            return $comment->user;
        });

        $include = $posts->merge($users);

        return [
            "links" => [
                "self" => route('comments.index'),
            ],

            "included" => $include->map(function ($item) {
                if ($item instanceof Post)
                    return new PostResource($item);
                elseif ($item instanceof User)
                    return new UserResource($item);
            }),
        ];
    }
}
