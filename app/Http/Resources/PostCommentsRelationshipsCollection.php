<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCommentsRelationshipsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $post = $this->additional["post"];

        return [
            'links' => [
                'self' => route('posts.relationships.comments', ['post' => $post]),
                'related' => route('posts.comments', ['post' => $post]),
            ],

            'data' => CommentIdentifierResource::collection($this->collection),
        ];
    }
}
