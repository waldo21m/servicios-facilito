<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->getTable(),
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'email' => $this->email,
            ],

            $this->mergeWhen(($this->isPostsLoaded() && $this->isCommentsLoaded()), [
                'relationships' => new UsersRelationshipResource($this),
            ]),

            'links' => [
                'self' => route('users.show', ['user' => $this->id]),
            ],
        ];
    }
}
