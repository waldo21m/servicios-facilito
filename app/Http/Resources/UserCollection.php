<?php

namespace App\Http\Resources;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function toArray($request)
    {
        return UserResource::collection($this->collection);
    }

    public function with($request)
    {
        $posts = $this->collection->flatMap(function ($user) {
            return $user->posts;
        });

        $comments = $this->collection->flatMap(function ($user) {
            return $user->comments;
        });

        $include = $posts->merge($comments);

        return [
            "links" => [
                "self" => route('users.index'),
            ],

            "included" => $include->map(function ($item) {
                if ($item instanceof Post)
                    return new PostResource($item);
                elseif ($item instanceof Comment)
                    return new CommentResource($item);
            }),
        ];
    }
}
