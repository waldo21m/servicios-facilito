<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UsersRelationshipResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'posts' => (new UserPostsRelationshipCollection($this->posts))->additional(["user" => $this]),

            'comments' => (new UserCommentsRelationshipCollection($this->comments))->additional(["user" => $this]),
        ];
    }
}
