<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserPostsRelationshipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->additional["user"];

        return [
            'links' => [
                'self' => route('users.relationships.posts', ['user' => $user]),
                'related' => route('users.posts', ['user' => $user]),
            ],

            'data' => PostIdentifierResource::collection($this->collection),
        ];
    }
}
