<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'author_id' => 'required'
        ];
    }

    /*
     * Laravel devuelve un objeto JSON con un status 422 de forma automática, sin embargo,
     * se realiza este override para modificarlo en caso de que haga falta.
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            "message" => "The given data was invalid.",
            "errors" => $validator->errors(),
            "old_inputs" => request()->all()
        ], 422));
    }
}
