<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $baseUrl = "/api/v1/";

    public function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    public function signIn()
    {
        Passport::actingAs(create('App\User', [
            'name' => 'Eduardo',
            'email' => 'eduardo20.3263@gmail.com',
            'password' => bcrypt('laravel'),
        ]));
    }
}
