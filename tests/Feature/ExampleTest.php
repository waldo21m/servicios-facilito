<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @test
     */
    public function see_the_marquez_signature()
    {
        $response = $this->json('GET', "/api/signature");
        $response->assertStatus(200);

        $response->assertJson([
            "made_by" => [
                "name" => "Eduardo Márquez",
                "dni" => "V21343328",
                "country" => "Venezuela",
                "state" => "Caracas",
                "address" => "Av. Ppal de Altavista, Conjunto Res. La Fundación, I Etapa, Res. 6, PB, Apto. 1-D",
                "phone" => "+584142409579",
                "university" => "UNEXPO L.C.M",
            ],
            "special_thanks" => "All my family, my girlfriend, my friends and the codigo facilito staff",
        ]);
    }
}
