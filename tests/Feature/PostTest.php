<?php

namespace Tests\Feature;

use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_post()
    {
        create('App\Models\Post', [
            'title' => "Python",
            'content' => "The most easier for make machine learning",
            'author_id' => 1,
        ]);

        $response = $this->json('GET', $this->baseUrl . "posts");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "posts",
                    "id" => 1,
                    "attributes" => [
                        "title" => "Python",
                        "content" => "The most easier for make machine learning",
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function index_post_page_two()
    {
        create('App\Models\Post', [
            'title' => "Python",
            'content' => "The most easier for make machine learning",
            'author_id' => 1,
        ]);

        create('App\Models\Post', [
            'title' => "Laravel",
            'content' => "The old reliable",
            'author_id' => 1,
        ]);

        $response = $this->json('GET', $this->baseUrl . "posts?page=1");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "posts",
                    "id" => 1,
                    "attributes" => [
                        "title" => "Python",
                        "content" => "The most easier for make machine learning",
                    ],
                ],
            ],
        ]);

        $response = $this->json('GET', $this->baseUrl . "posts?page=2");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "posts",
                    "id" => 2,
                    "attributes" => [
                        "title" => "Laravel",
                        "content" => "The old reliable",
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function index_shows_a_default_message_if_the_posts_list_is_empty()
    {
        $response = $this->json('GET', $this->baseUrl . "posts");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => "There are no registered posts",
        ]);
    }

    /**
     * @test
     */
    public function stores_post()
    {
        $user = create('App\User');

        $data = [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'content' => $this->faker->text($maxNbChars = 40),
            'author_id' => $user->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "posts", $data);

        $response->assertStatus(200);
        $this->assertDatabaseHas('posts', $data);

        $post = Post::all()->first();

        $response->assertJson([
            'id' => $post->id,
            'attributes' => [
                'title' => $post->title,
            ],
        ]);
    }

    /**
     * @test
     */
    public function deletes_post()
    {
        create('App\User');
        $post = create('App\Models\Post');

        $this->json('DELETE', $this->baseUrl . "posts/{$post->id}")->assertStatus(204);

        $this->assertNull(Post::find($post->id));
    }

    /**
     * @test
     */
    public function updates_post()
    {
        $data = [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'content' => $this->faker->text($maxNbChars = 40),
        ];

        create('App\User');
        $post = create('App\Models\Post');

        $response = $this->json('PUT', $this->baseUrl . "posts/{$post->id}", $data);
        $response->assertStatus(200);

        $post = $post->fresh();

        $this->assertEquals($post->title, $data['title']);
        $this->assertEquals($post->content, $data['content']);
    }

    /**
     * @test
     */
    public function updates_post_that_dont_exists()
    {
        $data = [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'content' => $this->faker->text($maxNbChars = 40),
        ];

        create('App\User');

        $response = $this->json('PUT', $this->baseUrl . "posts/999", $data);
        $response->assertStatus(404);

        $response->assertJson([
            "error" => "Model not found",
        ]);
    }

    /**
     * @test
     */
    public function shows_post()
    {
        create('App\User');
        $post = create('App\Models\Post');

        $response = $this->json('GET', $this->baseUrl . "posts/{$post->id}");
        $response->assertStatus(200);

        $response->assertJson([
            'id' => $post->id,
            'attributes' => [
                'title' => $post->title,
            ],
        ]);
    }

    /**
     * @test
     */
    public function shows_displays_a_404_error_if_the_post_not_found()
    {
        $response = $this->json('GET', $this->baseUrl . "posts/999");
        $response->assertStatus(404);

        $response->assertJson([
            "error" => "Model not found",
        ]);
    }
}
