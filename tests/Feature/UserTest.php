<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_user()
    {
        create('App\User', [
            'name' => 'Eduardo',
            'email' => 'emarquez@gmail.com',
            'password' => bcrypt('laravel'),
        ]);

        create('App\User', [
            'name' => 'Denger',
            'email' => 'dzamora@gmail.com',
            'password' => bcrypt('laravel'),
        ]);

        $response = $this->json('GET', $this->baseUrl . "users");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "users",
                    "id" => 1,
                    "attributes" => [
                        "name" => "Eduardo",
                        "email" => "eduardo20.3263@gmail.com"
                    ],
                ],
                [
                    "type" => "users",
                    "id" => 2,
                    "attributes" => [
                        "name" => "Eduardo",
                        "email" => "emarquez@gmail.com"
                    ],
                ],
                [
                    "type" => "users",
                    "id" => 3,
                    "attributes" => [
                        "name" => "Denger",
                        "email" => "dzamora@gmail.com"
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function index_user_page_two()
    {
        factory(User::class, 14)->create();

        create('App\User', [
            'name' => 'Eduardo',
            'email' => 'emarquez@gmail.com',
            'password' => bcrypt('laravel'),
        ]);

        $response = $this->json('GET', $this->baseUrl . "users?page=1");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "users",
                    "id" => 1,
                    "attributes" => [
                        "name" => "Eduardo",
                        "email" => "eduardo20.3263@gmail.com"
                    ],
                ],
            ],
        ]);

        $response = $this->json('GET', $this->baseUrl . "users?page=2");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "users",
                    "id" => 16,
                    "attributes" => [
                        "name" => "Eduardo",
                        "email" => "emarquez@gmail.com"
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(201);

        unset($data['password_confirmation']);

        $this->assertDatabaseHas('users', $data);

        /*
         * Como estamos usando laravel passport, automáticamente debe crear un
         * usuario para que funcione el método actingAs de la clase Passport.
         * Por eso, se escoge el último usuario.
         */
        $user = User::all()->last();

        $response->assertJson([
            'id' => $user->id,
            'attributes' => [
                'name' => $user->name,
                'email' => $user->email,
            ],
        ]);
    }

    /**
     * @test
     */
    public function shows_user()
    {
        $user = create('App\User');

        $response = $this->json('GET', $this->baseUrl . "users/{$user->id}");
        $response->assertStatus(200);

        $response->assertJson([
            'id' => $user->id,
            'attributes' => [
                'name' => $user->name,
                'email' => $user->email,
            ],
        ]);
    }

    /**
     * @test
     */
    public function shows_displays_a_404_error_if_the_user_not_found()
    {
        $response = $this->json('GET', $this->baseUrl . "users/999");
        $response->assertStatus(404);

        $response->assertJson([
            "error" => "Model not found",
        ]);
    }

    /**
     * @test
     */
    public function updates_user()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $user = create('App\User');

        $response = $this->json('PUT', $this->baseUrl . "users/{$user->id}", $data);
        $response->assertStatus(200);

        $user = $user->fresh();

        $this->assertEquals($user->name, $data['name']);
        $this->assertEquals($user->email, $data['email']);
    }

    /**
     * @test
     */
    public function updates_user_that_dont_exists()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('PUT', $this->baseUrl . "users/999", $data);
        $response->assertStatus(404);

        $response->assertJson([
            "error" => "Model not found",
        ]);
    }

    /**
     * @test
     */
    public function deletes_user()
    {
        $user = create('App\User');

        $this->json('DELETE', $this->baseUrl . "users/{$user->id}")->assertStatus(204);

        $this->assertNull(User::find($user->id));

        $this->assertSoftDeleted('users', $user->toArray());
    }
}
