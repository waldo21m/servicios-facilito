<?php

namespace Tests\Feature;

use App\Models\Comment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_comment()
    {
        $user = create('App\User');
        $post = create('App\Models\Post');

        create('App\Models\Comment', [
            "content" => "This is nice",
            "user_id" => $user->id,
            "post_id" => $post->id,
        ]);

        $response = $this->json('GET', $this->baseUrl . "comments");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "comments",
                    "id" => 1,
                    "attributes" => [
                        "content" => "This is nice",
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function index_comment_page_two()
    {
        $user = create('App\User');
        $post = create('App\Models\Post');

        create('App\Models\Comment', [
            "content" => "This is nice",
            "user_id" => $user->id,
            "post_id" => $post->id,
        ]);

        factory(Comment::class, 14)->create();

        create('App\Models\Comment', [
            "content" => "This is really nice",
            "user_id" => $user->id,
            "post_id" => $post->id,
        ]);

        $response = $this->json('GET', $this->baseUrl . "comments?page=1");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "comments",
                    "id" => 1,
                    "attributes" => [
                        "content" => "This is nice",
                    ],
                ],
            ],
        ]);

        $response = $this->json('GET', $this->baseUrl . "comments?page=2");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => [
                [
                    "type" => "comments",
                    "id" => 16,
                    "attributes" => [
                        "content" => "This is really nice",
                    ],
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function index_shows_a_default_message_if_the_comments_list_is_empty()
    {
        $response = $this->json('GET', $this->baseUrl . "comments");
        $response->assertStatus(200);

        $response->assertJson([
            "data" => "There are no registered comments",
        ]);
    }

    /**
     * @test
     */
    public function stores_comment()
    {
        $user = create('App\User');

        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
            'post_id' => $post->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(201);
        $this->assertDatabaseHas('comments', $data);

        $comment = Comment::all()->first();

        $response->assertJson([
            'id' => $comment->id,
            'attributes' => [
                'content' => $comment->content,
            ]
        ]);
    }

    /**
     * @test
     */
    public function shows_comment()
    {
        create('App\User');
        create('App\Models\Post');
        $comment = create('App\Models\Comment');

        $response = $this->json('GET', $this->baseUrl . "comments/{$comment->id}");
        $response->assertStatus(200);

        $response->assertJson([
            'id' => $comment->id,
            'attributes' => [
                'content' => $comment->content,
            ],
        ]);
    }

    /**
     * @test
     */
    public function shows_displays_a_404_error_if_the_comment_not_found()
    {
        $response = $this->json('GET', $this->baseUrl . "comments/999");
        $response->assertStatus(404);

        $response->assertJson([
            "error" => "Model not found",
        ]);
    }

    /**
     * @test
     */
    public function updates_comment()
    {
        $user = create('App\User');
        $post = create('App\Models\Post');
        $comment = create('App\Models\Comment');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
            'post_id' => $post->id,
        ];

        $response = $this->json('PUT', $this->baseUrl . "comments/{$comment->id}", $data);
        $response->assertStatus(200);

        $comment = $comment->fresh();

        $this->assertEquals($comment->content, $data['content']);
        $this->assertEquals($comment->user_id, $data['user_id']);
        $this->assertEquals($comment->post_id, $data['post_id']);
    }

    /**
     * @test
     */
    public function updates_comment_that_dont_exists()
    {
        $user = create('App\User');
        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
            'post_id' => $post->id,
        ];

        $response = $this->json('PUT', $this->baseUrl . "comments/999", $data);
        $response->assertStatus(404);

        $response->assertJson([
            "error" => "Model not found",
        ]);
    }

    /**
     * @test
     */
    public function deletes_comment()
    {
        create('App\User');
        create('App\Models\Post');
        $comment = create('App\Models\Comment');

        $this->json('DELETE', $this->baseUrl . "comments/{$comment->id}")->assertStatus(204);

        $this->assertNull(Comment::find($comment->id));
    }
}
