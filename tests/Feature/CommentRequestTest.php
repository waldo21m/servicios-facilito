<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentRequestTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function stores_comment_content_field_is_required()
    {
        $user = create('App\User');

        $post = create('App\Models\Post');

        $data = [
            'user_id' => $user->id,
            'post_id' => $post->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "content" => ["The content field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_post_field_is_required()
    {
        $user = create('App\User');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "post_id" => ["The post id field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_post_field_must_be_integer()
    {
        $user = create('App\User');

        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
            'post_id' => $post->content,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "post_id" => ["The post id must be an integer."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_post_field_must_be_greater_than_zero()
    {
        $user = create('App\User');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
            'post_id' => 0,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "post_id" => ["The post id must be at least 1."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_post_field_must_be_exists_in_post_table()
    {
        $user = create('App\User');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->id,
            'post_id' => 1,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "post_id" => ["The selected post id is invalid."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_user_field_is_required()
    {
        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'post_id' => $post->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "user_id" => ["The user id field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_user_field_must_be_integer()
    {
        $user = create('App\User');

        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => $user->name,
            'post_id' => $post->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "user_id" => ["The user id must be an integer."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_user_field_must_be_greater_than_zero()
    {
        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => 0,
            'post_id' => $post->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "user_id" => ["The user id must be at least 1."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_comment_user_field_must_be_exists_in_user_table()
    {
        $post = create('App\Models\Post');

        $data = [
            'content' => $this->faker->text($maxNbChars = 200),
            'user_id' => 999,
            'post_id' => $post->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "comments", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "user_id" => ["The selected user id is invalid."],
            ],
        ]);
    }
}
