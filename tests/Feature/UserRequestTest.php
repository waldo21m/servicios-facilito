<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserRequestTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function stores_user_name_field_is_required()
    {
        $password = bcrypt('laravel');

        $data = [
            'email' => $this->faker->unique()->safeEmail,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "name" => ["The name field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_name_field_is_too_long()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->paragraphs($nb = 10, $asText = true),
            'email' => $this->faker->unique()->safeEmail,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "name" => ["The name may not be greater than 255 characters."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_email_field_is_required()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "email" => ["The email field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_email_field_is_too_long_and_invalid()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => 'laravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravellaravel@gmail.com',
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "email" => [
                    "The email must be a valid email address.",
                    "The email may not be greater than 255 characters.",
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_email_field_must_be_unique()
    {
        $user = create('App\User');

        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "email" => [
                    "The email has already been taken.",
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_password_field_is_required()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "password" => ["The password field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_password_field_must_be_a_string_and_is_too_short()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => 12345,
            'password_confirmation' => 12345,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "password" => [
                    "The password must be a string.",
                    "The password must be at least 6 characters.",
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_user_password_field_must_be_confirmed()
    {
        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $password,
        ];

        $response = $this->json('POST', $this->baseUrl . "users", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "password" => [
                    "The password confirmation does not match.",
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function updates_user_email_field_must_be_the_same()
    {
        $user = create('App\User');

        $password = bcrypt('laravel');

        $data = [
            'name' => $this->faker->name,
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $user = create('App\User');

        $response = $this->json('PUT', $this->baseUrl . "users/{$user->id}", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "email" => [
                    "The email has already been taken.",
                ],
            ],
        ]);
    }
}
