<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostRequestTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function stores_post_title_field_is_required()
    {
        $user = create('App\User');

        $data = [
            'content' => $this->faker->text($maxNbChars = 40),
            'author_id' => $user->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "posts", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "title" => ["The title field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_post_content_field_is_required()
    {
        $user = create('App\User');

        $data = [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'author_id' => $user->id,
        ];

        $response = $this->json('POST', $this->baseUrl . "posts", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "content" => ["The content field is required."],
            ],
        ]);
    }

    /**
     * @test
     */
    public function stores_post_author_field_is_required()
    {
        $data = [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'content' => $this->faker->text($maxNbChars = 40),
        ];

        $response = $this->json('POST', $this->baseUrl . "posts", $data);

        $response->assertStatus(422);

        $response->assertJson([
            "errors" => [
                "author_id" => ["The author id field is required."],
            ],
        ]);
    }
}
