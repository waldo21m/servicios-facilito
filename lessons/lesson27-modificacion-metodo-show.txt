Modificación método show

En esta lección vamos a eliminar el apartado relationships porque puede ser
que no lo utilicemos. Para ello usaremos un método llamado relationLoaded
en el modelo de Post que nos devolverá un verdadero o falso si estamos usando
un Eager loading.