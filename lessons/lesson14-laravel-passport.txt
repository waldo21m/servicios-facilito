Laravel passport

La documentación la podemos encontrar en:
https://laravel.com/docs/5.5/passport

Lo primero que debemos hacer es instalarlo con el comando:
composer require laravel/passport=~4.0

P.D En laravel 5.5 LTS da un error al instalarlo, es necesario instalar
previamente este paquete:
composer require paragonie/random_compat:2.*

Laravel passport crea una serie de tablas, por lo cual debemos ejecutar el
comando:
php artisan migrate

Y lo siguiente es ejecutar el comando:
php artisan passport:install