Configurando entorno

Antes de ponernos a programar y generar las pruebas unitarias vamos a configurar
el entorno. Algo que nos puede resultar muy útil es crear un helper que nos
permite generar modelos unicamente pasando como argumento la ruta de la clase.
Para ello creamos la carpeta Utilities y dentro creamos el helper.php. Este
helper debemos agregar en el autoload con el archivo composer.json en el apartado
de autoload-dev y ejecutar el comando
composer dump-autoload

Ahora para ejecutar nuestras pruebas lo ejecutaremos en un entorno de desarrollo
de sqlite, para ello crearemos el driver de sqlite_testing en database.php y
para que tome este driver cuando se ejecute la prueba debemos configurarlo en
el archivo phpunit.xml

Para finalizar, crearemos una propiedad que será una Url base para ejecutar los
tests (o los queries).